import csv
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport

transport = AIOHTTPTransport(url="https://graphigo.prd.galaxy.eco/query")
client = Client(transport=transport, fetch_schema_from_transport=True)

query = gql(
    """
query nftHolders ($campaign_id: ID!, $after: String!){
  campaign(id: $campaign_id) {
    holders(first: 1000, after: $after) {
      totalCount
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
      list {
        address
      }
    }
  }
}
"""
)

def fetch(campaign_id):
  after = ""
  output_buff = list()

  while True:
    print("A")
    result = client.execute(query, variable_values={"campaign_id": campaign_id, "after": after})
    page_info = result["campaign"]["holders"]["pageInfo"]
    print(page_info)
    output_buff.extend([x["address"] for x in result["campaign"]["holders"]["list"]])

    if not page_info['hasNextPage']:
      return output_buff
    
    after = page_info["endCursor"]

output_buff = list()

with open('./campaigns.csv') as f:
  reader = csv.reader(f)
  for name, campaign_id in reader:
      print(name, campaign_id)
      address_list = fetch(campaign_id)

      output_buff.extend([(name, campaign_id, x) for x in address_list])

with open('./out.csv', 'w') as f:
  writer = csv.writer(f)
  writer.writerows(output_buff)